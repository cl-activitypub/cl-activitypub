;;;; Copyright 2018 John A. Whitley
;;;;
;;;; Licensed under the Apache License, Version 2.0 (the "License");
;;;; you may not use this file except in compliance with the License.
;;;; You may obtain a copy of the License at
;;;;      http://www.apache.org/licenses/LICENSE-2.0
;;;;  Unless required by applicable law or agreed to in writing, software
;;;; distributed under the License is distributed on an "AS IS" BASIS,
;;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;;; See the License for the specific language governing permissions and
;;;; limitations under the License.

(defpackage :cl-activitypub.activitystreams
  (:use :cl)
  (:export :object
	   :object-id
	   :object-type
	   :object-name
	   :object-attribution
	   :object-audience
	   :object-content
	   :object-ending
	   :object-generator
	   :object-context
	   :object-replying-to
	   :object-location
	   :object-publish-date
	   :object-replies
	   :object-starting-time
	   :object-summary
	   :object-tags
	   :object-last-update
	   :object-links
	   :object-to
	   :object-secret-to
	   :object-cc
	   :object-secret-cc
	   :object-media-type
	   :object-duration
	   :link
	   :link-address
	   :link-ref
	   :link-mediatype
	   :link-name
	   :link-width
	   :link-height
	   :get-link-dimensions
	   :intransitiveactivity
	   :activity-actor
	   :activity-object
	   :activity-target
	   :activity-result
	   :activity-origin
	   :activity-instrument
	   :activity
	   :collection
	   :collection-page
	   :collection-first-page
	   :collection-last-page
	   :collection-items
	   :collection-total-items
	   :recount-collection-items
	   :orderedcollection
	   :collectionpage
	   :collectionpage-parent
	   :collectionpage-next-page
	   :collectionpage-previous-page
	   :orderedcollectionpage))

(defpackage :cl-activitypub.activitystreams.extended
  (:use :cl-activitypub.activitystreams :cl)
  (:export :accept
	   :add
	   :create
	   :remove
	   :undo
	   :update
	   :delete*
	   :follow
	   :ignore*
	   :join
	   :leave
	   :like
	   :offer
	   :reject
	   :view
	   :listen
	   :read
	   :move
	   :announce
	   :flag
	   :dislike
	   :arrive
	   :travel
	   :tentativeaccept
	   :tentativereject
	   :block
	   :invite
	   :question
	   :question-oneof
	   :question-anyof
	   :question-close-time
	   :application
	   :group
	   :organization
	   :person
	   :service
	   :place
	   :place-accuracy
	   :place-height
	   :place-latitude
	   :place-longitude
	   :place-radius
	   :place-units
	   :relationship
	   :relation-subject
	   :relation-object
	   :relative
	   :article
	   :document
	   :note
	   :event
	   :audio
	   :image
	   :video
	   :page
	   :mention
	   :profile
	   :profile-describes
	   :tombstone
	   :tombstone-former-type
	   :tombstone-delete-date))
