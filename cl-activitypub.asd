(defsystem cl-activitypub
  :description "A simple ActivityPub server written in CL."
  :version "0r1"
  :author "John Whitley <dogjaw2233@protonmail.com>"
  :license "Apache 2.0"
  :components ((:file "packages")
	       (:module "activitystreams"
			:depends-on "packages"
			:serial t
			:components ((:file "activitystreams")
				     (:file "activitystreams-ext"))))
  :in-order-to ((test-op (test-op cl-activitypub-test))))

(defsystem cl-activitypub-test
  :depends-on (:cl-activitypub
	       :prove)
  :defsystem-depends-on (:prove-asdf)
  :components ((:test-file "streamtest"))
  :perform (test-op :after (op c)
		    (funcall (intern #.(string :run) :prove) c)))
