;;;; Copyright 2018 John A. Whitley
;;;;
;;;; Licensed under the Apache License, Version 2.0 (the "License");
;;;; you may not use this file except in compliance with the License.
;;;; You may obtain a copy of the License at
;;;;      http://www.apache.org/licenses/LICENSE-2.0
;;;;  Unless required by applicable law or agreed to in writing, software
;;;; distributed under the License is distributed on an "AS IS" BASIS,
;;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;;; See the License for the specific language governing permissions and
;;;; limitations under the License.

(in-package :cl-activitypub.activitystreams.extended)

(defmacro define-slotless-class-with-type (name parent)
  `(defclass ,name (,parent)
     ((type :initform (string-capitalize
			(string-downcase
			 (symbol-name ',name)))))))

(defmacro define-slotless-classes-with-type (parent &body names)
  `(progn ,@(loop for i in names collect
		 `(define-slotless-class-with-type ,i ,parent))))

(define-slotless-classes-with-type activity accept add create remove undo update
				   delete* follow ignore* join leave like offer
				   reject view listen read move announce flag
				   dislike)
					;; LISP macros: five minutes of intensive
					;; design followed by an eternity of
                                        ;; profound laziness.
(define-slotless-classes-with-type intransitiveactivity arrive travel)

(define-slotless-class-with-type tentativeaccept accept)
(define-slotless-class-with-type tentativereject reject)
(define-slotless-class-with-type block ignore*)
(define-slotless-class-with-type invite offer)

(defclass question (intransitiveactivity)
  ((type :initform "Question")
   (oneof :initform nil
	  :initarg oneof
	  :accessor question-oneof)
   (anyof :initform nil
	  :initarg anyof
	  :accessor question-anyof)
   (closed :initform nil
	   :initarg closed
	   :accessor question-close-time)))

(define-slotless-classes-with-type object application group organization person
				   service)
(defclass place (object)
  ((type :initform "Place")
   (accuracy :initform nil
	     :initarg :accuracy
	     :accessor place-accuracy)
   (altitude :initform nil
	     :initarg :altitude
	     :accessor place-height)
   (latitude :initform nil
	     :initarg :latitude
	     :accessor place-latitude)
   (longitude :initform nil
	      :initarg :longitude
	      :accessor place-longitude)
   (radius :initform nil
	   :initarg :radius
	   :accessor place-radius)
   (units :initform nil
	  :initarg :units
	  :accessor place-units)))

(defclass relationship (object)
  ((type :initform "Relationship")
   (subject :initform nil
	    :initarg :subject
	    :accessor relation-subject)
   (object :initform nil
	   :initarg :object
	   :accessor relation-object)
   (relationship :initform nil
		 :initarg :relationship
		 :accessor relative)))

(define-slotless-classes-with-type object article document note event)
(define-slotless-classes-with-type document audio image video page)
(define-slotless-class-with-type mention link)

(defclass profile (object)
  ((type :initform "Profile")
   (describes :initarg :describes
	      :accessor profile-describes)))

(defclass tombstone (object)
  ((type :initform "Tombstone")
   (formertype :initarg :formertype
	       :accessor tombstone-former-type)
   (deleted :initform nil
	    :initarg :deleted
	    :accessor tombstone-delete-date)))
